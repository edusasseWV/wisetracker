import React, { useState, useEffect } from 'react';
import MainToolbar from './MainToolbar';
import {
  makeStyles, Paper, Toolbar, TextField, IconButton, Button,
} from '@material-ui/core';

import Map from './map/Map';
import SelectedDeviceMap from './map/SelectedDeviceMap';
import AccuracyMap from './map/AccuracyMap';
import GeofenceMap from './map/GeofenceMap';
import CurrentPositionsMap from './map/CurrentPositionsMap';
import CurrentLocationMap from './map/CurrentLocationMap';
import t from './common/localization';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  }
}));

const MainPage = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <MainToolbar />
      <Map>
        <CurrentLocationMap />
        <GeofenceMap />
        <AccuracyMap />
        <CurrentPositionsMap />
        <SelectedDeviceMap />
      </Map>
    
    </div>
  );
};

export default MainPage;
