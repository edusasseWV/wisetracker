const wisetrackerWhite = '#FFF';
const wisetrackerBlue = '#34bdeb';
const wisetrackerRed = '#CC2222';
const wisetrackerGray = '#888888';

export default {
  common: {
    blue: wisetrackerBlue,
    red: wisetrackerRed,
    gray: wisetrackerGray,
  },
  primary: {
    main: wisetrackerWhite,
  },
  secondary: {
    main: wisetrackerBlue,
    contrastText: wisetrackerWhite,
  },
  special: {
    main: wisetrackerGray,
    contrastText: wisetrackerWhite,
  },
  colors: {
    red: {
      color: wisetrackerRed,
    },
    blue: {
      color: wisetrackerBlue,
    },
    gray: {
      color: wisetrackerGray,
    },
  },
};
