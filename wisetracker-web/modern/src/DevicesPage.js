import React, { useState } from 'react';
import {
  TableContainer, Table, TableRow, TableCell, TableHead, TableBody, makeStyles, IconButton,
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import t from './common/localization';
import { useEffectAsync } from './reactHelper';
import EditCollectionView from './EditCollectionView';
import { prefixString } from './common/stringUtils';
import { formatBoolean } from './common/formatter';
import OptionsLayout from './settings/OptionsLayout';

const useStyles = makeStyles((theme) => ({
  columnAction: {
    width: theme.spacing(1),
    padding: theme.spacing(0, 1),
  },
}));

const DevicesView = ({ updateTimestamp, onMenuClick }) => {
  const classes = useStyles();

  const [items, setItems] = useState([]);

  useEffectAsync(async () => {
    const response = await fetch('/api/devices');
    if (response.ok) {
      setItems(await response.json());
    }
  }, [updateTimestamp]);

  const formatList = (prefix, value) => {
    if (value) {
      return value
        .split(/[, ]+/)
        .filter(Boolean)
        .map((it) => t(prefixString(prefix, it)))
        .join(', ');
    }
    return '';
  };

  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell className={classes.columnAction} />
            <TableCell>{t('sharedName')}</TableCell>
            <TableCell>{t('deviceIdentifier')}</TableCell>
            <TableCell>{t('action')}</TableCell> 
          </TableRow>
        </TableHead>
        <TableBody>
          {items.map((item) => (
            <TableRow key={item.id}>
              
              <TableCell>{t(prefixString('event', item.name))}</TableCell>
              <TableCell>{item.name}</TableCell> 
              <TableCell>{item.uniqueId}</TableCell> 
              <TableCell className={classes.columnAction} padding="none">
                <IconButton onClick={(event) => onMenuClick(event.currentTarget, item.id)}>
                  <MoreVertIcon />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const DevicesPage = () => (
  <OptionsLayout>
    <EditCollectionView content={DevicesView} editPath="/device" endpoint="devices" />
  </OptionsLayout>
);

export default DevicesPage;
