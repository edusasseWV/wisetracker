
Ext.define('WiseTracker.model.KnownNotificator', {
    extend: 'Ext.data.Model',
    idProperty: 'type',

    fields: [{
        name: 'type',
        type: 'string'
    }, {
        name: 'name',
        convert: function (v, rec) {
            return WiseTracker.app.getNotificatorString(rec.get('type'));
        },
        depends: ['type']
    }]
});
