
Ext.define('WiseTracker.model.KnownNotification', {
    extend: 'Ext.data.Model',
    idProperty: 'type',

    fields: [{
        name: 'type',
        type: 'string'
    }, {
        name: 'name',
        convert: function (v, rec) {
            return WiseTracker.app.getEventString(rec.get('type'));
        },
        depends: ['type']
    }]
});
