
Ext.define('WiseTracker.model.KnownCommand', {
    extend: 'Ext.data.Model',
    idProperty: 'type',

    fields: [{
        name: 'type',
        type: 'string'
    }, {
        name: 'name',
        convert: function (v, rec) {
            return WiseTracker.AttributeFormatter.getFormatter('commandType')(rec.get('type'));
        },
        depends: ['type']
    }, {
        name: 'parameters'
    }]
});
