

Ext.define('WiseTracker.view.BaseWindow', {
    extend: 'Ext.window.Window',

    width: WiseTracker.Style.windowWidth,
    height: WiseTracker.Style.windowHeight,
    layout: 'fit',

    initComponent: function () {
        if (window.innerWidth < WiseTracker.Style.windowWidth || window.innerHeight < WiseTracker.Style.windowHeight) {
            this.maximized = true;
            this.style = 'border-width: 0';
        }
        this.callParent();
    }
});
