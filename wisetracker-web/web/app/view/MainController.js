
Ext.define('WiseTracker.view.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.mainController',

    init: function () {
        this.lookupReference('reportView').setHidden(WiseTracker.app.getBooleanAttributePreference('ui.disableReport'));
        this.lookupReference('eventsView').setHidden(WiseTracker.app.getBooleanAttributePreference('ui.disableEvents'));
    }
});
