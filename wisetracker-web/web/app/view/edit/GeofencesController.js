

Ext.define('WiseTracker.view.edit.GeofencesController', {
    extend: 'WiseTracker.view.edit.ToolbarController',
    alias: 'controller.geofences',

    requires: [
        'WiseTracker.view.dialog.Geofence',
        'WiseTracker.model.Geofence'
    ],

    objectModel: 'WiseTracker.model.Geofence',
    objectDialog: 'WiseTracker.view.dialog.Geofence',
    removeTitle: Strings.sharedGeofence
});
