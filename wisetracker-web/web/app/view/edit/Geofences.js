

Ext.define('WiseTracker.view.edit.Geofences', {
    extend: 'WiseTracker.view.GridPanel',
    xtype: 'geofencesView',

    requires: [
        'WiseTracker.view.edit.GeofencesController',
        'WiseTracker.view.edit.Toolbar'
    ],

    controller: 'geofences',
    store: 'Geofences',

    tbar: {
        xtype: 'editToolbar'
    },

    listeners: {
        selectionchange: 'onSelectionChange'
    },

    columns: {
        defaults: {
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal
        },
        items: [{
            text: Strings.sharedName,
            dataIndex: 'name',
            filter: 'string'
        }, {
            text: Strings.sharedDescription,
            dataIndex: 'description',
            filter: 'string'
        }, {
            text: Strings.sharedCalendar,
            dataIndex: 'calendarId',
            hidden: true,
            filter: {
                type: 'list',
                labelField: 'name',
                store: 'AllCalendars'
            },
            renderer: WiseTracker.AttributeFormatter.getFormatter('calendarId')
        }]
    }
});
