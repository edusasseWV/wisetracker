

Ext.define('WiseTracker.view.edit.ComputedAttributesController', {
    extend: 'WiseTracker.view.edit.ToolbarController',
    alias: 'controller.computedAttributes',

    requires: [
        'WiseTracker.view.dialog.ComputedAttribute',
        'WiseTracker.model.ComputedAttribute'
    ],

    objectModel: 'WiseTracker.model.ComputedAttribute',
    objectDialog: 'WiseTracker.view.dialog.ComputedAttribute',
    removeTitle: Strings.sharedComputedAttribute
});
