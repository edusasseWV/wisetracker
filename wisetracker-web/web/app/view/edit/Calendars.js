

Ext.define('WiseTracker.view.edit.Calendars', {
    extend: 'WiseTracker.view.GridPanel',
    xtype: 'calendarsView',

    requires: [
        'WiseTracker.view.edit.CalendarsController',
        'WiseTracker.view.edit.Toolbar'
    ],

    controller: 'calendars',
    store: 'Calendars',

    tbar: {
        xtype: 'editToolbar'
    },

    listeners: {
        selectionchange: 'onSelectionChange'
    },

    columns: {
        defaults: {
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal
        },
        items: [{
            text: Strings.sharedName,
            dataIndex: 'name',
            filter: 'string'
        }]
    }
});
