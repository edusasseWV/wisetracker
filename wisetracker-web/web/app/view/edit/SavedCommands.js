

Ext.define('WiseTracker.view.edit.SavedCommands', {
    extend: 'WiseTracker.view.GridPanel',
    xtype: 'savedCommandsView',

    requires: [
        'WiseTracker.view.edit.SavedCommandsController',
        'WiseTracker.view.edit.Toolbar'
    ],

    controller: 'savedCommands',
    store: 'Commands',

    tbar: {
        xtype: 'editToolbar'
    },

    listeners: {
        selectionchange: 'onSelectionChange'
    },

    columns: {
        defaults: {
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal
        },
        items: [{
            text: Strings.sharedDescription,
            dataIndex: 'description',
            filter: 'string'
        }, {
            text: Strings.sharedType,
            dataIndex: 'type',
            filter: {
                type: 'list',
                idField: 'type',
                labelField: 'name',
                store: 'AllCommandTypes'
            },
            renderer: WiseTracker.AttributeFormatter.getFormatter('commandType')
        }, {
            text: Strings.commandSendSms,
            dataIndex: 'textChannel',
            renderer: WiseTracker.AttributeFormatter.getFormatter('textChannel'),
            filter: 'boolean'
        }]
    }
});
