

Ext.define('WiseTracker.view.edit.NotificationsController', {
    extend: 'WiseTracker.view.edit.ToolbarController',
    alias: 'controller.notifications',

    requires: [
        'WiseTracker.view.dialog.Notification',
        'WiseTracker.model.Notification'
    ],

    objectModel: 'WiseTracker.model.Notification',
    objectDialog: 'WiseTracker.view.dialog.Notification',
    removeTitle: Strings.sharedNotification
});
