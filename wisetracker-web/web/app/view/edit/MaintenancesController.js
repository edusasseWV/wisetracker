

Ext.define('WiseTracker.view.edit.MaintenancesController', {
    extend: 'WiseTracker.view.edit.ToolbarController',
    alias: 'controller.maintenances',

    requires: [
        'WiseTracker.view.dialog.Maintenance',
        'WiseTracker.model.Maintenance'
    ],

    objectModel: 'WiseTracker.model.Maintenance',
    objectDialog: 'WiseTracker.view.dialog.Maintenance',
    removeTitle: Strings.sharedMaintenance
});
