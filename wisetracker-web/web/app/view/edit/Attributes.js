

Ext.define('WiseTracker.view.edit.Attributes', {
    extend: 'WiseTracker.view.GridPanel',
    xtype: 'attributesView',

    requires: [
        'WiseTracker.view.edit.AttributesController',
        'WiseTracker.view.edit.Toolbar'
    ],

    controller: 'attributes',

    tbar: {
        xtype: 'editToolbar'
    },

    listeners: {
        selectionchange: 'onSelectionChange'
    },

    columns: {
        defaults: {
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal
        },
        items: [{
            text: Strings.sharedName,
            dataIndex: 'name',
            filter: 'string',
            renderer: function (value) {
                var attribute;
                if (this.attributesStore) {
                    attribute = Ext.getStore(this.attributesStore).getById(value);
                }
                return attribute && attribute.get('name') || value;
            }
        }, {
            text: Strings.stateValue,
            dataIndex: 'value',
            renderer: function (value, metaData, record) {
                var attribute;
                if (this.attributesStore) {
                    attribute = Ext.getStore(this.attributesStore).getById(record.get('name'));
                }
                return WiseTracker.AttributeFormatter.renderAttribute(value, attribute);
            }
        }]
    }
});
