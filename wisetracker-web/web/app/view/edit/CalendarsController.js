

Ext.define('WiseTracker.view.edit.CalendarsController', {
    extend: 'WiseTracker.view.edit.ToolbarController',
    alias: 'controller.calendars',

    requires: [
        'WiseTracker.view.dialog.Calendar',
        'WiseTracker.model.Calendar'
    ],

    objectModel: 'WiseTracker.model.Calendar',
    objectDialog: 'WiseTracker.view.dialog.Calendar',
    removeTitle: Strings.sharedCalendar
});
