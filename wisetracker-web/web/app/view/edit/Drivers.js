

Ext.define('WiseTracker.view.edit.Drivers', {
    extend: 'WiseTracker.view.GridPanel',
    xtype: 'driversView',

    requires: [
        'WiseTracker.view.edit.DriversController',
        'WiseTracker.view.edit.Toolbar'
    ],

    controller: 'drivers',
    store: 'Drivers',

    tbar: {
        xtype: 'editToolbar'
    },

    listeners: {
        selectionchange: 'onSelectionChange'
    },

    columns: {
        defaults: {
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal
        },
        items: [{
            text: Strings.sharedName,
            dataIndex: 'name',
            filter: 'string'
        }, {
            text: Strings.deviceIdentifier,
            dataIndex: 'uniqueId',
            filter: 'string'
        }]
    }
});
