

Ext.define('WiseTracker.view.edit.DriversController', {
    extend: 'WiseTracker.view.edit.ToolbarController',
    alias: 'controller.drivers',

    requires: [
        'WiseTracker.view.dialog.Driver',
        'WiseTracker.model.Driver'
    ],

    objectModel: 'WiseTracker.model.Driver',
    objectDialog: 'WiseTracker.view.dialog.Driver',
    removeTitle: Strings.sharedDriver
});
