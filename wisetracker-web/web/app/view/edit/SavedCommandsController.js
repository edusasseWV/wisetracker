

Ext.define('WiseTracker.view.edit.SavedCommandsController', {
    extend: 'WiseTracker.view.edit.ToolbarController',
    alias: 'controller.savedCommands',

    requires: [
        'WiseTracker.view.dialog.SavedCommand',
        'WiseTracker.model.Command'
    ],

    objectModel: 'WiseTracker.model.Command',
    objectDialog: 'WiseTracker.view.dialog.SavedCommand',
    removeTitle: Strings.sharedSavedCommand
});
