

Ext.define('WiseTracker.view.dialog.Base', {
    extend: 'Ext.window.Window',

    bodyPadding: WiseTracker.Style.normalPadding,
    resizable: false,
    scrollable: true,
    constrain: true,

    initComponent: function () {
        if (window.innerHeight) {
            this.maxHeight = window.innerHeight - WiseTracker.Style.normalPadding * 2;
        }
        this.callParent();
    }
});
