

Ext.define('WiseTracker.view.dialog.GeofenceController', {
    extend: 'WiseTracker.view.dialog.BaseEditController',
    alias: 'controller.geofence',

    requires: [
        'WiseTracker.view.BaseWindow',
        'WiseTracker.view.map.GeofenceMap'
    ],

    config: {
        listen: {
            controller: {
                '*': {
                    savearea: 'saveArea'
                }
            }
        }
    },

    init: function () {
        this.lookupReference('calendarCombo').setHidden(
            WiseTracker.app.getBooleanAttributePreference('ui.disableCalendars'));
    },

    saveArea: function (value) {
        this.lookupReference('areaField').setValue(value);
    },

    onAreaClick: function (button) {
        var dialog, record;
        dialog = button.up('window').down('form');
        record = dialog.getRecord();
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedArea,
            items: {
                xtype: 'geofenceMapView',
                area: record.get('area')
            }
        }).show();
    }
});
