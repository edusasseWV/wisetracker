

Ext.define('WiseTracker.view.dialog.Group', {
    extend: 'WiseTracker.view.dialog.BaseEdit',

    requires: [
        'WiseTracker.view.ClearableComboBox',
        'WiseTracker.view.UnescapedTextField'
    ],

    title: Strings.groupDialog,

    items: {
        xtype: 'form',
        items: [{
            xtype: 'fieldset',
            title: Strings.sharedRequired,
            items: [{
                xtype: 'unescapedTextField',
                name: 'name',
                fieldLabel: Strings.sharedName,
                allowBlank: false
            }]
        }, {
            xtype: 'fieldset',
            title: Strings.sharedExtra,
            collapsible: true,
            collapsed: true,
            items: [{
                xtype: 'clearableComboBox',
                name: 'groupId',
                fieldLabel: Strings.groupParent,
                store: 'Groups',
                queryMode: 'local',
                displayField: 'name',
                valueField: 'id'
            }]
        }]
    }
});
