

Ext.define('WiseTracker.view.dialog.DeviceController', {
    extend: 'WiseTracker.view.dialog.BaseEditController',
    alias: 'controller.device',

    init: function () {
        if (WiseTracker.app.getUser().get('administrator')) {
            this.lookupReference('disabledField').setHidden(false);
        }
    }

});
