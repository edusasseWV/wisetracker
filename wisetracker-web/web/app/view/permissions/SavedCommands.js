

Ext.define('WiseTracker.view.permissions.SavedCommands', {
    extend: 'WiseTracker.view.permissions.Base',
    xtype: 'linkSavedCommandsView',

    columns: {
        items: [{
            text: Strings.sharedDescription,
            dataIndex: 'description',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            filter: 'string'
        }, {
            text: Strings.sharedType,
            dataIndex: 'type',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            filter: {
                type: 'list',
                idField: 'type',
                labelField: 'name',
                store: 'AllCommandTypes'
            },
            renderer: WiseTracker.AttributeFormatter.getFormatter('commandType')
        }, {
            text: Strings.commandSendSms,
            dataIndex: 'textChannel',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            filter: 'boolean'
        }]
    }
});
