
Ext.define('WiseTracker.view.permissions.Base', {
    extend: 'WiseTracker.view.GridPanel',

    requires: [
        'WiseTracker.view.permissions.BaseController'
    ],

    controller: 'base',

    selModel: {
        selType: 'checkboxmodel',
        checkOnly: true,
        showHeaderCheckbox: false
    },

    listeners: {
        beforedeselect: 'onBeforeDeselect',
        beforeselect: 'onBeforeSelect'
    }
});
