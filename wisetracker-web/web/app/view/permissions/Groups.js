

Ext.define('WiseTracker.view.permissions.Groups', {
    extend: 'WiseTracker.view.permissions.Base',
    xtype: 'linkGroupsView',

    requires: [
        'WiseTracker.AttributeFormatter'
    ],

    columns: {
        items: [{
            text: Strings.sharedName,
            dataIndex: 'name',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            filter: 'string'
        }, {
            text: Strings.groupDialog,
            dataIndex: 'groupId',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            hidden: true,
            filter: {
                type: 'list',
                labelField: 'name',
                store: 'AllGroups'
            },
            renderer: WiseTracker.AttributeFormatter.getFormatter('groupId')
        }]
    }
});
