

Ext.define('WiseTracker.view.permissions.Geofences', {
    extend: 'WiseTracker.view.permissions.Base',
    xtype: 'linkGeofencesView',

    columns: {
        items: [{
            text: Strings.sharedName,
            dataIndex: 'name',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            filter: 'string'
        }, {
            text: Strings.sharedCalendar,
            dataIndex: 'calendarId',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            hidden: true,
            filter: {
                type: 'list',
                labelField: 'name',
                store: 'AllCalendars'
            },
            renderer: WiseTracker.AttributeFormatter.getFormatter('calendarId')
        }]
    }
});
