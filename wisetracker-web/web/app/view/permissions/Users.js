

Ext.define('WiseTracker.view.permissions.Users', {
    extend: 'WiseTracker.view.permissions.Base',
    xtype: 'linkUsersView',

    columns: {
        items: [{
            text: Strings.sharedName,
            dataIndex: 'name',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            filter: 'string'
        }]
    }
});
