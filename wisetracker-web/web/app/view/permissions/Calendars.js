

Ext.define('WiseTracker.view.permissions.Calendars', {
    extend: 'WiseTracker.view.permissions.Base',
    xtype: 'linkCalendarsView',

    columns: {
        items: [{
            text: Strings.sharedName,
            dataIndex: 'name',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            filter: 'string'
        }]
    }
});
