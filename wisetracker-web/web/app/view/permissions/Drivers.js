

Ext.define('WiseTracker.view.permissions.Drivers', {
    extend: 'WiseTracker.view.permissions.Base',
    xtype: 'linkDriversView',

    columns: {
        items: [{
            text: Strings.sharedName,
            dataIndex: 'name',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            filter: 'string'
        }, {
            text: Strings.deviceIdentifier,
            dataIndex: 'uniqueId',
            flex: 1,
            minWidth: WiseTracker.Style.columnWidthNormal,
            filter: 'string'
        }]
    }
});
