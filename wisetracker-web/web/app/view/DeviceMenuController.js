

Ext.define('WiseTracker.view.DeviceMenuController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.deviceMenu',

    requires: [
        'WiseTracker.view.permissions.Geofences',
        'WiseTracker.view.permissions.Drivers',
        'WiseTracker.view.permissions.Notifications',
        'WiseTracker.view.edit.ComputedAttributes',
        'WiseTracker.view.permissions.SavedCommands',
        'WiseTracker.view.permissions.Maintenances',
        'WiseTracker.view.dialog.DeviceAccumulators',
        'WiseTracker.view.BaseWindow'
    ],

    init: function () {
        this.lookupReference('menuDriversButton').setHidden(
            WiseTracker.app.getVehicleFeaturesDisabled() || WiseTracker.app.getBooleanAttributePreference('ui.disableDrivers'));
        this.lookupReference('menuComputedAttributesButton').setHidden(
            WiseTracker.app.getBooleanAttributePreference('ui.disableComputedAttributes'));
        this.lookupReference('menuCommandsButton').setHidden(WiseTracker.app.getPreference('limitCommands', false));
        this.lookupReference('menuDeviceAccumulatorsButton').setHidden(
            !WiseTracker.app.getUser().get('administrator') && WiseTracker.app.getUser().get('userLimit') === 0 || WiseTracker.app.getVehicleFeaturesDisabled());
        this.lookupReference('menuMaintenancesButton').setHidden(
            WiseTracker.app.getVehicleFeaturesDisabled() || WiseTracker.app.getBooleanAttributePreference('ui.disableMaintenance'));
    },

    onGeofencesClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedGeofences,
            items: {
                xtype: 'linkGeofencesView',
                baseObjectName: 'deviceId',
                linkObjectName: 'geofenceId',
                storeName: 'Geofences',
                baseObject: this.getView().up('deviceMenu').device.getId()
            }
        }).show();
    },

    onNotificationsClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedNotifications,
            items: {
                xtype: 'linkNotificationsView',
                baseObjectName: 'deviceId',
                linkObjectName: 'notificationId',
                storeName: 'Notifications',
                baseObject: this.getView().up('deviceMenu').device.getId()
            }
        }).show();
    },

    onComputedAttributesClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedComputedAttributes,
            items: {
                xtype: 'linkComputedAttributesView',
                baseObjectName: 'deviceId',
                linkObjectName: 'attributeId',
                storeName: 'ComputedAttributes',
                baseObject: this.getView().up('deviceMenu').device.getId()
            }
        }).show();
    },

    onDriversClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedDrivers,
            items: {
                xtype: 'linkDriversView',
                baseObjectName: 'deviceId',
                linkObjectName: 'driverId',
                storeName: 'Drivers',
                baseObject: this.getView().up('deviceMenu').device.getId()
            }
        }).show();
    },

    onCommandsClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedSavedCommands,
            items: {
                xtype: 'linkSavedCommandsView',
                baseObjectName: 'deviceId',
                linkObjectName: 'commandId',
                storeName: 'Commands',
                baseObject: this.getView().up('deviceMenu').device.getId()
            }
        }).show();
    },

    onMaintenancesClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedMaintenance,
            items: {
                xtype: 'linkMaintenancesView',
                baseObjectName: 'deviceId',
                linkObjectName: 'maintenanceId',
                storeName: 'Maintenances',
                baseObject: this.getView().up('deviceMenu').device.getId()
            }
        }).show();
    },

    onDeviceAccumulatorsClick: function () {
        var position, dialog = Ext.create('WiseTracker.view.dialog.DeviceAccumulators');
        dialog.deviceId = this.getView().up('deviceMenu').device.getId();
        position = Ext.getStore('LatestPositions').findRecord('deviceId', dialog.deviceId, 0, false, false, true);
        if (position) {
            dialog.lookupReference('totalDistance').setValue(position.get('attributes').totalDistance);
            dialog.lookupReference('hours').setValue(position.get('attributes').hours);
        }
        dialog.show();
    }
});
