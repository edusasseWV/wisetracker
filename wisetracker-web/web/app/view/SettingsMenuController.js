

Ext.define('WiseTracker.view.SettingsMenuController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.settings',

    requires: [
        'WiseTracker.view.dialog.LoginController',
        'WiseTracker.view.dialog.User',
        'WiseTracker.view.dialog.Server',
        'WiseTracker.view.edit.Users',
        'WiseTracker.view.edit.Groups',
        'WiseTracker.view.edit.Geofences',
        'WiseTracker.view.edit.Drivers',
        'WiseTracker.view.edit.Notifications',
        'WiseTracker.view.edit.ComputedAttributes',
        'WiseTracker.view.Statistics',
        'WiseTracker.view.edit.Calendars',
        'WiseTracker.view.edit.SavedCommands',
        'WiseTracker.view.edit.Maintenances',
        'WiseTracker.view.BaseWindow'
    ],

    init: function () {
        var admin, manager, readonly;
        admin = WiseTracker.app.getUser().get('administrator');
        manager = WiseTracker.app.getUser().get('userLimit') !== 0;
        readonly = WiseTracker.app.getPreference('readonly', false);
        if (admin) {
            this.lookupReference('settingsServerButton').setHidden(false);
            this.lookupReference('settingsStatisticsButton').setHidden(false);
            this.lookupReference('settingsComputedAttributesButton').setHidden(
                WiseTracker.app.getBooleanAttributePreference('ui.disableComputedAttributes'));
        }
        if (admin || manager) {
            this.lookupReference('settingsUsersButton').setHidden(false);
        }
        if (admin || !readonly) {
            this.lookupReference('settingsUserButton').setHidden(false);
            this.lookupReference('settingsGroupsButton').setHidden(false);
            this.lookupReference('settingsGeofencesButton').setHidden(false);
            this.lookupReference('settingsNotificationsButton').setHidden(false);
            this.lookupReference('settingsCalendarsButton').setHidden(
                WiseTracker.app.getBooleanAttributePreference('ui.disableCalendars'));
            this.lookupReference('settingsDriversButton').setHidden(
                WiseTracker.app.getVehicleFeaturesDisabled() || WiseTracker.app.getBooleanAttributePreference('ui.disableDrivers'));
            this.lookupReference('settingsCommandsButton').setHidden(WiseTracker.app.getPreference('limitCommands', false));
            this.lookupReference('settingsMaintenancesButton').setHidden(
                WiseTracker.app.getVehicleFeaturesDisabled() || WiseTracker.app.getBooleanAttributePreference('ui.disableMaintenance'));
        }
    },

    onUserClick: function () {
        var dialog = Ext.create('WiseTracker.view.dialog.User', {
            selfEdit: true
        });
        dialog.down('form').loadRecord(WiseTracker.app.getUser());
        dialog.lookupReference('testNotificationButton').setHidden(false);
        dialog.show();
    },

    onGroupsClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.settingsGroups,
            items: {
                xtype: 'groupsView'
            }
        }).show();
    },

    onGeofencesClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedGeofences,
            items: {
                xtype: 'geofencesView'
            }
        }).show();
    },

    onServerClick: function () {
        var dialog = Ext.create('WiseTracker.view.dialog.Server');
        dialog.down('form').loadRecord(WiseTracker.app.getServer());
        dialog.show();
    },

    onUsersClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.settingsUsers,
            items: {
                xtype: 'usersView'
            }
        }).show();
    },

    onNotificationsClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedNotifications,
            items: {
                xtype: 'notificationsView'
            }
        }).show();
    },

    onComputedAttributesClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedComputedAttributes,
            items: {
                xtype: 'computedAttributesView'
            }
        }).show();
    },

    onStatisticsClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.statisticsTitle,
            items: {
                xtype: 'statisticsView'
            }
        }).show();
    },

    onCalendarsClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedCalendars,
            items: {
                xtype: 'calendarsView'
            }
        }).show();
    },

    onDriversClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedDrivers,
            items: {
                xtype: 'driversView'
            }
        }).show();
    },

    onCommandsClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedSavedCommands,
            items: {
                xtype: 'savedCommandsView'
            }
        }).show();
    },

    onMaintenancesClick: function () {
        Ext.create('WiseTracker.view.BaseWindow', {
            title: Strings.sharedMaintenance,
            items: {
                xtype: 'maintenancesView'
            }
        }).show();
    },

    onLogoutClick: function () {
        Ext.create('WiseTracker.view.dialog.LoginController').logout();
    }
});
