

Ext.define('WiseTracker.view.CustomTimeField', {
    extend: 'Ext.form.field.Time',
    xtype: 'customTimeField',

    constructor: function (config) {
        if (WiseTracker.app.getPreference('twelveHourFormat', false)) {
            config.format = WiseTracker.Style.timeFormat12;
        } else {
            config.format = WiseTracker.Style.timeFormat24;
        }
        this.callParent(arguments);
    }
});
