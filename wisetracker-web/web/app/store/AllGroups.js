

Ext.define('WiseTracker.store.AllGroups', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Group',

    proxy: {
        type: 'rest',
        url: 'api/groups',
        extraParams: {
            all: true
        }
    }
});
