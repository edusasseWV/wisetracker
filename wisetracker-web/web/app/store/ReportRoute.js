

Ext.define('WiseTracker.store.ReportRoute', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Position',

    proxy: {
        type: 'rest',
        url: 'api/reports/route',
        timeout: WiseTracker.Style.reportTimeout,
        headers: {
            'Accept': 'application/json'
        },
        listeners: {
            exception: function (proxy, exception) {
                WiseTracker.app.showError(exception);
            }
        }
    }
});
