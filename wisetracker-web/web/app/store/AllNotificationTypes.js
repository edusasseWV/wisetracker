

Ext.define('WiseTracker.store.AllNotificationTypes', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.KnownNotification',

    proxy: {
        type: 'rest',
        url: 'api/notifications/types',
        listeners: {
            exception: function (proxy, response) {
                WiseTracker.app.showError(response);
            }
        }
    }
});
