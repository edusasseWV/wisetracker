

Ext.define('WiseTracker.store.Attributes', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Attribute',

    sorters: [{
        property: 'priority'
    }]
});
