

Ext.define('WiseTracker.store.Calendars', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Calendar',

    proxy: {
        type: 'rest',
        url: 'api/calendars',
        writer: {
            writeAllFields: true
        }
    }
});
