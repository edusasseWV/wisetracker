

Ext.define('WiseTracker.store.LatestPositions', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Position'
});
