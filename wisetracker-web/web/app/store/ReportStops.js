

Ext.define('WiseTracker.store.ReportStops', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.ReportStop',

    proxy: {
        type: 'rest',
        url: 'api/reports/stops',
        timeout: WiseTracker.Style.reportTimeout,
        headers: {
            'Accept': 'application/json'
        },
        listeners: {
            exception: function (proxy, exception) {
                WiseTracker.app.showError(exception);
            }
        }
    }
});
