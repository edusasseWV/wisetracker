

Ext.define('WiseTracker.store.AllNotifications', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Notification',

    proxy: {
        type: 'rest',
        url: 'api/notifications',
        extraParams: {
            all: true
        }
    }
});
