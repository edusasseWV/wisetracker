

Ext.define('WiseTracker.store.Users', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.User',

    proxy: {
        type: 'rest',
        url: 'api/users',
        writer: {
            writeAllFields: true
        }
    }
});
