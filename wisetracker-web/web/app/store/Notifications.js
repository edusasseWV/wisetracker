

Ext.define('WiseTracker.store.Notifications', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Notification',

    proxy: {
        type: 'rest',
        url: 'api/notifications',
        writer: {
            writeAllFields: true
        }
    }
});
