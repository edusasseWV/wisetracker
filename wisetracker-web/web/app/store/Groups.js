

Ext.define('WiseTracker.store.Groups', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Group',

    proxy: {
        type: 'rest',
        url: 'api/groups',
        writer: {
            writeAllFields: true
        }
    }
});
