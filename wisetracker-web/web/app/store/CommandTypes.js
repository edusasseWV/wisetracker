

Ext.define('WiseTracker.store.CommandTypes', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.KnownCommand',

    proxy: {
        type: 'rest',
        url: 'api/commands/types',
        listeners: {
            'exception': function (proxy, response) {
                WiseTracker.app.showError(response);
            }
        }
    }
});
