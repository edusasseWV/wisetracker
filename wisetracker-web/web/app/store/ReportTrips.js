

Ext.define('WiseTracker.store.ReportTrips', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.ReportTrip',

    proxy: {
        type: 'rest',
        url: 'api/reports/trips',
        timeout: WiseTracker.Style.reportTimeout,
        headers: {
            'Accept': 'application/json'
        },
        listeners: {
            exception: function (proxy, exception) {
                WiseTracker.app.showError(exception);
            }
        }
    }
});
