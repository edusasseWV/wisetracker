

Ext.define('WiseTracker.store.Devices', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Device',

    proxy: {
        type: 'rest',
        url: 'api/devices',
        writer: {
            writeAllFields: true
        }
    }
});
