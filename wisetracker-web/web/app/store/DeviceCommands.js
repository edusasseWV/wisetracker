

Ext.define('WiseTracker.store.DeviceCommands', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Command',

    proxy: {
        type: 'rest',
        url: 'api/commands/send',
        listeners: {
            'exception': function (proxy, response) {
                WiseTracker.app.showError(response);
            }
        }
    }
});
