

Ext.define('WiseTracker.store.Statistics', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Statistics',

    proxy: {
        type: 'rest',
        url: 'api/statistics'
    }
});
