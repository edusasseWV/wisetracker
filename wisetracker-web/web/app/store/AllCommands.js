

Ext.define('WiseTracker.store.AllCommands', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Command',

    proxy: {
        type: 'rest',
        url: 'api/commands',
        extraParams: {
            all: true
        }
    }
});
