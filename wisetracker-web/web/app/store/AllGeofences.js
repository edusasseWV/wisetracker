

Ext.define('WiseTracker.store.AllGeofences', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Geofence',

    proxy: {
        type: 'rest',
        url: 'api/geofences',
        extraParams: {
            all: true
        }
    }
});
