
Ext.define('WiseTracker.store.CommonDeviceAttributes', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.KnownAttribute',

    data: [{
        key: 'speedLimit',
        name: Strings.attributeSpeedLimit,
        valueType: 'number',
        dataType: 'speed'
    }, {
        key: 'report.ignoreOdometer',
        name: Strings.attributeReportIgnoreOdometer,
        valueType: 'boolean'
    }]
});
