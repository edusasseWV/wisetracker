

Ext.define('WiseTracker.store.Events', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Event',

    trackRemoved: false,

    proxy: {
        type: 'rest',
        url: 'api/events'
    }
});
