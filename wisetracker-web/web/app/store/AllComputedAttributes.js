

Ext.define('WiseTracker.store.AllComputedAttributes', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.ComputedAttribute',

    proxy: {
        type: 'rest',
        url: 'api/attributes/computed',
        extraParams: {
            all: true
        }
    }
});
