

Ext.define('WiseTracker.store.Commands', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Command',

    proxy: {
        type: 'rest',
        url: 'api/commands',
        writer: {
            writeAllFields: true
        }
    }
});
