

Ext.define('WiseTracker.store.Positions', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Position',

    proxy: {
        type: 'rest',
        url: 'api/positions',
        headers: {
            'Accept': 'application/json'
        }
    }
});
