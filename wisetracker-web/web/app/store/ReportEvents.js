

Ext.define('WiseTracker.store.ReportEvents', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Event',

    proxy: {
        type: 'rest',
        url: 'api/reports/events',
        timeout: WiseTracker.Style.reportTimeout,
        headers: {
            'Accept': 'application/json'
        },
        listeners: {
            exception: function (proxy, exception) {
                WiseTracker.app.showError(exception);
            }
        }
    }
});
