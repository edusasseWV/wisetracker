

Ext.define('WiseTracker.store.AllCalendars', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Calendar',

    proxy: {
        type: 'rest',
        url: 'api/calendars',
        extraParams: {
            all: true
        }
    }
});
