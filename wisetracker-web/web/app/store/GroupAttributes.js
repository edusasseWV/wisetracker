
Ext.define('WiseTracker.store.GroupAttributes', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.KnownAttribute',

    data: [{
        key: 'processing.copyAttributes',
        name: Strings.attributeProcessingCopyAttributes,
        valueType: 'string'
    }, {
        key: 'decoder.timezone',
        name: Strings.sharedTimezone,
        valueType: 'string',
        dataType: 'timezone'
    }]
});
