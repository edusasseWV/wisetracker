

Ext.define('WiseTracker.store.ComputedAttributes', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.ComputedAttribute',

    proxy: {
        type: 'rest',
        url: 'api/attributes/computed',
        writer: {
            writeAllFields: true
        }
    }
});
