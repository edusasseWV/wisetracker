

Ext.define('WiseTracker.store.AllDevices', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Device',

    proxy: {
        type: 'rest',
        url: 'api/devices',
        extraParams: {
            all: true
        }
    }
});
