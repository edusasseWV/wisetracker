

Ext.define('WiseTracker.store.EventPositions', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Position',

    trackRemoved: false,

    proxy: {
        type: 'rest',
        url: 'api/positions',
        headers: {
            'Accept': 'application/json'
        }
    }
});
