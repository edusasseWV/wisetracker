

Ext.define('WiseTracker.store.Geofences', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.Geofence',

    proxy: {
        type: 'rest',
        url: 'api/geofences',
        writer: {
            writeAllFields: true
        }
    }
});
