

Ext.define('WiseTracker.store.ReportSummary', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.ReportSummary',

    proxy: {
        type: 'rest',
        url: 'api/reports/summary',
        timeout: WiseTracker.Style.reportTimeout,
        headers: {
            'Accept': 'application/json'
        },
        listeners: {
            exception: function (proxy, exception) {
                WiseTracker.app.showError(exception);
            }
        }
    }
});
