

Ext.define('WiseTracker.store.AllNotificators', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.KnownNotificator',

    proxy: {
        type: 'rest',
        url: 'api/notifications/notificators',
        listeners: {
            exception: function (proxy, response) {
                WiseTracker.app.showError(response);
            }
        }
    }
});
