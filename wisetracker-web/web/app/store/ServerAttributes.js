
Ext.define('WiseTracker.store.ServerAttributes', {
    extend: 'Ext.data.Store',
    model: 'WiseTracker.model.KnownAttribute',

    data: []
});
