
package com.wisetracker.notificators;

import com.wisetracker.model.Event;
import com.wisetracker.model.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class NotificatorNull extends Notificator {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificatorNull.class);

    @Override
    public void sendAsync(long userId, Event event, Position position) {
        LOGGER.warn("You are using null notificatior, please check your configuration, notification not sent");
    }

    @Override
    public void sendSync(long userId, Event event, Position position) {
        LOGGER.warn("You are using null notificatior, please check your configuration, notification not sent");
    }

}
