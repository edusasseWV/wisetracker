
package com.wisetracker.notificators;

import com.wisetracker.model.Event;
import com.wisetracker.model.Position;
import com.wisetracker.notification.MessageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Notificator {

    private static final Logger LOGGER = LoggerFactory.getLogger(Notificator.class);

    public void sendAsync(final long userId, final Event event, final Position position) {
        new Thread(() -> {
            try {
                sendSync(userId, event, position);
            } catch (MessageException | InterruptedException error) {
                LOGGER.warn("Event send error", error);
            }
        }).start();
    }

    public abstract void sendSync(long userId, Event event, Position position)
        throws MessageException, InterruptedException;

}
