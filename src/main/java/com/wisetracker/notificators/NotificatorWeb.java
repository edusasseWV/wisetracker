
package com.wisetracker.notificators;

import com.wisetracker.model.Event;
import com.wisetracker.model.Position;
import com.wisetracker.Context;

public final class NotificatorWeb extends Notificator {

    @Override
    public void sendSync(long userId, Event event, Position position) {
        Context.getConnectionManager().updateEvent(userId, event);
    }

}
