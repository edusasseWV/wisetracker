
package com.wisetracker.notificators;

import com.wisetracker.model.Event;
import com.wisetracker.model.Position;
import com.wisetracker.notification.FullMessage;
import com.wisetracker.notification.MessageException;
import com.wisetracker.notification.NotificationFormatter;
import com.wisetracker.Context;

import javax.mail.MessagingException;

public final class NotificatorMail extends Notificator {

    @Override
    public void sendSync(long userId, Event event, Position position) throws MessageException {
        try {
            FullMessage message = NotificationFormatter.formatFullMessage(userId, event, position);
            Context.getMailManager().sendMessage(userId, message.getSubject(), message.getBody());
        } catch (MessagingException e) {
            throw new MessageException(e);
        }
    }

}
