
package com.wisetracker.notificators;

import com.wisetracker.Main;
import com.wisetracker.database.StatisticsManager;
import com.wisetracker.model.Event;
import com.wisetracker.model.Position;
import com.wisetracker.model.User;
import com.wisetracker.notification.MessageException;
import com.wisetracker.notification.NotificationFormatter;
import com.wisetracker.Context;

public final class NotificatorSms extends Notificator {

    @Override
    public void sendAsync(long userId, Event event, Position position) {
        final User user = Context.getPermissionsManager().getUser(userId);
        if (user.getPhone() != null) {
            Main.getInjector().getInstance(StatisticsManager.class).registerSms();
            Context.getSmsManager().sendMessageAsync(user.getPhone(),
                    NotificationFormatter.formatShortMessage(userId, event, position), false);
        }
    }

    @Override
    public void sendSync(long userId, Event event, Position position) throws MessageException, InterruptedException {
        final User user = Context.getPermissionsManager().getUser(userId);
        if (user.getPhone() != null) {
            Main.getInjector().getInstance(StatisticsManager.class).registerSms();
            Context.getSmsManager().sendMessageSync(user.getPhone(),
                    NotificationFormatter.formatShortMessage(userId, event, position), false);
        }
    }

}
