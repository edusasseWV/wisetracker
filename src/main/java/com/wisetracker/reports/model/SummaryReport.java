
package com.wisetracker.reports.model;

public class SummaryReport extends BaseReport {

    private long engineHours; // milliseconds

    public long getEngineHours() {
        return engineHours;
    }

    public void setEngineHours(long engineHours) {
        this.engineHours = engineHours;
    }
}
