
package com.wisetracker.geocoder;

public class GeocoderException extends RuntimeException {

    public GeocoderException(String message) {
        super(message);
    }

}
