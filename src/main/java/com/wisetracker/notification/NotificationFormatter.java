
package com.wisetracker.notification;

import com.wisetracker.model.Device;
import com.wisetracker.model.Event;
import com.wisetracker.model.Position;
import com.wisetracker.model.User;
import com.wisetracker.reports.ReportUtils;
import org.apache.velocity.VelocityContext;
import com.wisetracker.Context;

public final class NotificationFormatter {

    private NotificationFormatter() {
    }

    public static VelocityContext prepareContext(long userId, Event event, Position position) {

        User user = Context.getPermissionsManager().getUser(userId);
        Device device = Context.getIdentityManager().getById(event.getDeviceId());

        VelocityContext velocityContext = TextTemplateFormatter.prepareContext(user);

        velocityContext.put("device", device);
        velocityContext.put("event", event);
        if (position != null) {
            velocityContext.put("position", position);
            velocityContext.put("speedUnit", ReportUtils.getSpeedUnit(userId));
            velocityContext.put("distanceUnit", ReportUtils.getDistanceUnit(userId));
            velocityContext.put("volumeUnit", ReportUtils.getVolumeUnit(userId));
        }
        if (event.getGeofenceId() != 0) {
            velocityContext.put("geofence", Context.getGeofenceManager().getById(event.getGeofenceId()));
        }

        return velocityContext;
    }

    public static FullMessage formatFullMessage(long userId, Event event, Position position) {
        VelocityContext velocityContext = prepareContext(userId, event, position);
        return TextTemplateFormatter.formatFullMessage(velocityContext, event.getType());
    }

    public static String formatShortMessage(long userId, Event event, Position position) {
        VelocityContext velocityContext = prepareContext(userId, event, position);
        return TextTemplateFormatter.formatShortMessage(velocityContext, event.getType());
    }

}
