
package com.wisetracker.config;

public enum KeyType {
    GLOBAL,
    SERVER,
    USER,
    DEVICE,
}
