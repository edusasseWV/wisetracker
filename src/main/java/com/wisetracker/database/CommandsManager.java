
package com.wisetracker.database;

import com.wisetracker.model.Command;
import com.wisetracker.model.Position;
import com.wisetracker.model.Typed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.wisetracker.Context;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CommandsManager  extends ExtendedObjectManager<Command> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandsManager.class);

    private final Map<Long, Queue<Command>> deviceQueues = new ConcurrentHashMap<>();

    private final boolean queueing;

    public CommandsManager(DataManager dataManager, boolean queueing) {
        super(dataManager, Command.class);
        this.queueing = queueing;
    }

    public boolean checkDeviceCommand(long deviceId, long commandId) {
        return !getAllDeviceItems(deviceId).contains(commandId);
    }

    public boolean sendCommand(Command command) throws Exception {
        long deviceId = command.getDeviceId();
        if (command.getId() != 0) {
            command = getById(command.getId()).clone();
            command.setDeviceId(deviceId);
        }
        if (command.getTextChannel()) {
            Position lastPosition = Context.getIdentityManager().getLastPosition(deviceId);
            String phone = Context.getIdentityManager().getById(deviceId).getPhone();
            if (command.getType().equals(Command.TYPE_CUSTOM)) {
                if (Context.getSmsManager() != null) {
                    Context.getSmsManager().sendMessageSync(phone, command.getString(Command.KEY_DATA), true);
                } else {
                    throw new RuntimeException("SMS is not enabled");
                }
            } else {
                throw new RuntimeException("Command " + command.getType() + " is not supported");
            }
        } else {
            if (!queueing) {
                throw new RuntimeException("Device is not online");
            } else {
                getDeviceQueue(deviceId).add(command);
                return false;
            }
        }
        return true;
    }

    public Collection<Long> getSupportedCommands(long deviceId) {
        List<Long> result = new ArrayList<>();
        Position lastPosition = Context.getIdentityManager().getLastPosition(deviceId);
        for (long commandId : getAllDeviceItems(deviceId)) {
            Command command = getById(commandId);
            if (lastPosition != null) {

            } else if (command.getType().equals(Command.TYPE_CUSTOM)) {
                result.add(commandId);
            }
        }
        return result;
    }

    public Collection<Typed> getCommandTypes(long deviceId, boolean textChannel) {
        Position lastPosition = Context.getIdentityManager().getLastPosition(deviceId);
        if (lastPosition != null) {
            return getCommandTypes(lastPosition.getProtocol(), textChannel);
        } else {
            return Collections.singletonList(new Typed(Command.TYPE_CUSTOM));
        }
    }

    public Collection<Typed> getCommandTypes(String protocolName, boolean textChannel) {
        List<Typed> result = new ArrayList<>();
        return result;
    }

    public Collection<Typed> getAllCommandTypes() {
        List<Typed> result = new ArrayList<>();
        Field[] fields = Command.class.getDeclaredFields();
        for (Field field : fields) {
            if (Modifier.isStatic(field.getModifiers()) && field.getName().startsWith("TYPE_")) {
                try {
                    result.add(new Typed(field.get(null).toString()));
                } catch (IllegalArgumentException | IllegalAccessException error) {
                    LOGGER.warn("Get command types error", error);
                }
            }
        }
        return result;
    }

    private Queue<Command> getDeviceQueue(long deviceId) {
        Queue<Command> deviceQueue;
        try {
            readLock();
            deviceQueue = deviceQueues.get(deviceId);
        } finally {
            readUnlock();
        }
        if (deviceQueue != null) {
            return deviceQueue;
        } else {
            try {
                writeLock();
                return deviceQueues.computeIfAbsent(deviceId, key -> new ConcurrentLinkedQueue<>());
            } finally {
                writeUnlock();
            }
        }
    }

    public Collection<Command> readQueuedCommands(long deviceId) {
        return readQueuedCommands(deviceId, Integer.MAX_VALUE);
    }

    public Collection<Command> readQueuedCommands(long deviceId, int count) {
        Queue<Command> deviceQueue;
        try {
            readLock();
            deviceQueue = deviceQueues.get(deviceId);
        } finally {
            readUnlock();
        }
        Collection<Command> result = new ArrayList<>();
        if (deviceQueue != null) {
            Command command = deviceQueue.poll();
            while (command != null && result.size() < count) {
                result.add(command);
                command = deviceQueue.poll();
            }
        }
        return result;
    }

}
