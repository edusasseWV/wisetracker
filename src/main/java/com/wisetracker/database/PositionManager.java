
package com.wisetracker.database;

import com.wisetracker.Context;
import com.wisetracker.config.Config;
import com.wisetracker.config.Keys;
import com.wisetracker.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class PositionManager extends BaseObjectManager<Position> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PositionManager.class);

    private final Config config;
    private final long dataRefreshDelay;

    private Map<String, Position> positionsByUniqueId;
    private Map<String, Position> positionsByPhone;
    private final AtomicLong positionsLastUpdate = new AtomicLong();

    private final Map<Long, Position> positions = new ConcurrentHashMap<>();

    public PositionManager(DataManager dataManager) {
        super(dataManager, Position.class);
        this.config = Context.getConfig();
        try {
            writeLock();
            if (positionsByPhone == null) {
                positionsByPhone = new ConcurrentHashMap<>();
            }
            if (positionsByUniqueId == null) {
                positionsByUniqueId = new ConcurrentHashMap<>();
            }
        } finally {
            writeUnlock();
        }
        dataRefreshDelay = config.getLong(Keys.DATABASE_REFRESH_DELAY) * 1000;
    }


    public void updatePositionCache(boolean force) throws SQLException {
        long lastUpdate = positionsLastUpdate.get();
        if ((force || System.currentTimeMillis() - lastUpdate > dataRefreshDelay)
                && positionsLastUpdate.compareAndSet(lastUpdate, System.currentTimeMillis())) {
            refreshItems();
        }
    }

    public Position getByUniqueId(String uniqueId) throws SQLException {
        boolean forceUpdate;
        try {
            readLock();
            forceUpdate = !positionsByUniqueId.containsKey(uniqueId) && !config.getBoolean(Keys.DATABASE_IGNORE_UNKNOWN);
        } finally {
            readUnlock();
        }
        updatePositionCache(forceUpdate);
        try {
            readLock();
            return positionsByUniqueId.get(uniqueId);
        } finally {
            readUnlock();
        }
    }

    @Override
    public Set<Long> getAllItems() {
        Set<Long> result = super.getAllItems();
        if (result.isEmpty()) {
            try {
                updatePositionCache(true);
            } catch (SQLException e) {
                LOGGER.warn("Update position cache error", e);
            }
            result = super.getAllItems();
        }
        return result;
    }

    public Collection<Position> getAllPositions() {
        return getItems(getAllItems());
    }

    public void addPosition(Position position) {
        try {
            getDataManager().addObject(position);
        } catch (SQLException error) {
            LOGGER.warn("Event save error", error);
        }
    }


}
