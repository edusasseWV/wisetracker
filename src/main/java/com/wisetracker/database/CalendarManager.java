
package com.wisetracker.database;

import com.wisetracker.model.Calendar;

public class CalendarManager extends SimpleObjectManager<Calendar> {

    public CalendarManager(DataManager dataManager) {
        super(dataManager, Calendar.class);
    }

}
