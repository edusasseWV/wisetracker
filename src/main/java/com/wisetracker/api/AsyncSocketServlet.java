
package com.wisetracker.api;

import org.eclipse.jetty.websocket.server.JettyWebSocketServlet;
import org.eclipse.jetty.websocket.server.JettyWebSocketServletFactory;
import com.wisetracker.Context;
import com.wisetracker.api.resource.SessionResource;
import com.wisetracker.config.Keys;

import javax.servlet.http.HttpSession;
import java.time.Duration;

public class AsyncSocketServlet extends JettyWebSocketServlet {

    @Override
    public void configure(JettyWebSocketServletFactory factory) {
        factory.setIdleTimeout(Duration.ofMillis(Context.getConfig().getLong(Keys.WEB_TIMEOUT)));
        factory.setCreator((req, resp) -> {
            if (req.getSession() != null) {
                long userId = (Long) ((HttpSession) req.getSession()).getAttribute(SessionResource.USER_ID_KEY);
                return new AsyncSocket(userId);
            } else {
                return null;
            }
        });
    }

}
