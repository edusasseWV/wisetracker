
package com.wisetracker.api;

import com.wisetracker.Context;
import com.wisetracker.config.Keys;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import java.io.IOException;

public class CorsResponseFilter implements ContainerResponseFilter {

    private static final String ORIGIN_ALL = "*";
    private static final String HEADERS_ALL = "origin, content-type, accept, authorization";
    private static final String METHODS_ALL = "GET, POST, PUT, DELETE, OPTIONS";

    @Override
    public void filter(ContainerRequestContext request, ContainerResponseContext response) throws IOException {
        if (!response.getHeaders().containsKey("access-control-allow-headers".toString())) {
            response.getHeaders().add("access-control-allow-headers", HEADERS_ALL);
        }

        if (!response.getHeaders().containsKey("access-control-allow-credentials".toString())) {
            response.getHeaders().add("access-control-allow-credentials".toString(), true);
        }

        if (!response.getHeaders().containsKey("access-control-allow-methods".toString())) {
            response.getHeaders().add("access-control-allow-methods".toString(), METHODS_ALL);
        }

        if (!response.getHeaders().containsKey("access-control-allow-origin".toString())) {
            String origin = request.getHeaderString("origin".toString());
            String allowed = Context.getConfig().getString(Keys.WEB_ORIGIN);

            if (origin == null) {
                response.getHeaders().add("access-control-allow-origin".toString(), ORIGIN_ALL);
            } else if (allowed == null || allowed.equals(ORIGIN_ALL) || allowed.contains(origin)) {
                response.getHeaders().add("access-control-allow-origin".toString(), origin);
            }
        }
    }

}
