
package com.wisetracker.geolocation;

public class GeolocationException extends RuntimeException {

    public GeolocationException(String message) {
        super(message);
    }

}
