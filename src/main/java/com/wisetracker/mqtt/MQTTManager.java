package com.wisetracker.mqtt;

import org.apache.commons.lang3.StringUtils;
import com.wisetracker.config.Config;
import com.wisetracker.config.Keys;

public class MQTTManager {

    private String serverURI;
    private String username;
    private String password;

    private final Config config;

    private WiseTrackerClientMQTT client;
    WiseTrackerClientMQTTListener clientMQTTListener;

    public MQTTManager(Config config) {
        this.config = config;

        serverURI = config.getString(Keys.MQTT_URL);
        username = config.getString(Keys.MQTT_USER);
        password = config.getString(Keys.MQTT_PASSWORD);

        WiseTrackerClientMQTT client = null;
        if (!StringUtils.isEmpty(serverURI)) {
            try {
                client = new WiseTrackerClientMQTT(serverURI, username, password);
                client.start();
                this.clientMQTTListener = new WiseTrackerClientMQTTListener(client, "#", 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}