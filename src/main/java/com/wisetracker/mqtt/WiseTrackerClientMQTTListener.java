package com.wisetracker.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import com.wisetracker.Context;
import com.wisetracker.config.Keys;
import com.wisetracker.model.Device;
import com.wisetracker.model.Position;

import java.util.Date;

public class WiseTrackerClientMQTTListener implements IMqttMessageListener {

    public WiseTrackerClientMQTTListener(WiseTrackerClientMQTT wiseTrackerClientMQTT, String topico, int qos) {
        wiseTrackerClientMQTT.subscribe(qos, this, topico);
    }

    public void messageArrived(String topico, MqttMessage mm) throws Exception {
        final String msgStr = new String(mm.getPayload());
        String[] split = msgStr.split(";");
        if (split.length == 3) {
            Position position = new Position("MQTT");
            position.setDeviceId(Long.parseLong(split[0]));
            position.setLatitude(Double.parseDouble(split[1]));
            position.setLongitude(Double.parseDouble(split[2]));
            position.setTime(new Date());
            position.setDeviceTime(new Date());
            position.setFixTime(new Date());
            position.setServerTime(new Date());
            position.setSpeed(0);
            position.setValid(true);
            position.setAltitude(0);
            position.setSpeed(0d);
            position.setAddress(null);
            position.setCourse(0d);
            position.setAccuracy(1);
            position.setNetwork(null);

            if (Context.getConfig().getBoolean(Keys.DATABASE_SAVE_ORIGINAL)) {
                position.set(Position.KEY_ORIGINAL, msgStr);
            }

            Context.getDeviceManager().updateLatestPosition(position);
            Context.getConnectionManager().updateDevice(position.getDeviceId(), Device.STATUS_ONLINE, new Date());
        }
        System.out.println("Mensagem recebida:");
        System.out.println("\tTópico: " + topico);
        System.out.println("\tMensagem: " + msgStr);
        System.out.println("");
    }

}