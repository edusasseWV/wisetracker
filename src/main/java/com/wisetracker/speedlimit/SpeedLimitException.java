
package com.wisetracker.speedlimit;

public class SpeedLimitException extends RuntimeException {

    public SpeedLimitException(String message) {
        super(message);
    }

}
