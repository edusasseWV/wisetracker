
package com.wisetracker.sms;

import com.wisetracker.notification.MessageException;

public interface SmsManager {

    void sendMessageSync(
            String destAddress, String message, boolean command) throws InterruptedException, MessageException;

    void sendMessageAsync(
            String destAddress, String message, boolean command);

}
