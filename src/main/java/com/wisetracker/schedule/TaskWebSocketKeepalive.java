
package com.wisetracker.schedule;

import com.wisetracker.Context;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TaskWebSocketKeepalive implements Runnable {

    private static final long PERIOD_SECONDS = 55;

    public void schedule(ScheduledExecutorService executor) {
        executor.scheduleAtFixedRate(this, PERIOD_SECONDS, PERIOD_SECONDS, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        Context.getConnectionManager().sendKeepalive();
    }

}
