
package com.wisetracker.schedule;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class ScheduleManager {

    private ScheduledExecutorService executor;

    public void start() {
        executor = Executors.newSingleThreadScheduledExecutor();

        new TaskDeviceInactivityCheck().schedule(executor);
        new TaskWebSocketKeepalive().schedule(executor);
    }

    public void stop() {
        if (executor != null) {
            executor.shutdown();
            executor = null;
        }

    }

}
