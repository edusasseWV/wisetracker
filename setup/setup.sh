#!/bin/sh

PRESERVECONFIG=0
if [ -f /opt/wisetracker/conf/wisetracker.xml ]
then
    cp /opt/wisetracker/conf/wisetracker.xml /opt/wisetracker/conf/wisetracker.xml.saved
    PRESERVECONFIG=1
fi

mkdir -p /opt/wisetracker
cp -r * /opt/wisetracker
chmod -R go+rX /opt/wisetracker

if [ ${PRESERVECONFIG} -eq 1 ] && [ -f /opt/wisetracker/conf/wisetracker.xml.saved ]
then
    mv -f /opt/wisetracker/conf/wisetracker.xml.saved /opt/wisetracker/conf/wisetracker.xml
fi

mv /opt/wisetracker/wisetracker.service /etc/systemd/system
chmod 664 /etc/systemd/system/wisetracker.service

systemctl daemon-reload
systemctl enable wisetracker.service

rm /opt/wisetracker/setup.sh
rm -r ../out
