WiseTracker is a free and open source GPS tracking system.

Installation instructions are available on the official website:

Windows - https://www.wisetracker.org/windows/
Linux   - https://www.wisetracker.org/linux/
Docker  - https://www.wisetracker.org/docker/
Other   - https://www.wisetracker.org/manual-installation/

If you have any questions or problems visit support page:

https://www.wisetracker.org/support/
