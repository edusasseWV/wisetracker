[Setup]
AppName=WiseTracker
AppVersion=4.14
DefaultDirName={pf}\WiseTracker
OutputBaseFilename=wisetracker-setup
ArchitecturesInstallIn64BitMode=x64

[Dirs]
Name: "{app}\data"
Name: "{app}\logs"

[Files]
Source: "out\*"; DestDir: "{app}"; Flags: recursesubdirs

[Run]
Filename: "{app}\jre\bin\java.exe"; Parameters: "-jar ""{app}\wisetracker-server.jar"" --install .\conf\wisetracker.xml"; Flags: runhidden

[UninstallRun]
Filename: "{app}\jre\bin\java.exe"; Parameters: "-jar ""{app}\wisetracker-server.jar"" --uninstall"; Flags: runhidden
